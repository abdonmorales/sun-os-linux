# Sun/OS v2021.04
Sun/OS is a free operating system that run on the base of Arch Linux and has GNOME as it's default Desktop Environment

Source code is posted here, we included an Archiso so you build Sun/OS since currently we dont have a graphical installer and install .iso . So you have to build Sun/OS, we also have a CLI and GUI installer that comes seperately from the Archiso that you can **git clone**...we will also included a vhd and ova to boot in virtualbox

Here are the specs:

Kernel: Linux 5.11.X for rolling kernel and Sun Kernel v2.2.0

Version: v2021.03

Rolling Release

Package Manager: pacman and pamac

DE (Desktop Environment): GNOME 3.38

Architecture: x86_64 (AMD and Intel)

## Sun/OS Installer v2021.04
To get the Sun/OS Installer for your Sun/OS installation, go to this link for more info and for the source:
https://github.com/SunOS-Linux/SunOS-Installer

(C) 2021 Morales Research Corporation 

