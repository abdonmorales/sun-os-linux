## Sun/OS v2021.02
README und Deutsch

Sun/OS ist ein kostenloses Betriebssystem, das auf der Basis von Arch Linux ausgeführt wird und GNOME als Standard-Desktop-Umgebung verwendet

Der Quellcode ist hier veröffentlicht. Wir haben ein Archiso hinzugefügt, damit Sie Sun/OS erstellen können, da wir derzeit kein grafisches Installationsprogramm haben und .iso installieren. Sie müssen also Sun/OS erstellen. Wir haben auch ein CLI- und GUI-Installationsprogramm, das separat von Archiso stammt und das Sie ** git clone ** können. Wir werden auch ein vhd und ova zum Booten in virtualbox einbinden

Hier sind die Spezifikationen:

Kernel: Linux 5.10.15 für den rollenden Kernel

Version: v2021.02

Rolling Release

Paketmanager: Pacman und Pamac

DE (Desktop-Umgebung): GNOME 3.38

Architektur: x86_64 (AMD und Intel)

## Sun Installationsprogramm v2021.02
Um das Sun/OS-Installationsprogramm für Ihre Sun/OS-Installation zu erhalten, gehen Sie zu diesem Link, um weitere Informationen und die Quelle zu erhalten:
https://github.com/SunOS-Linux/SunOS-Installer

(C) 2021 Morales Research Corp
