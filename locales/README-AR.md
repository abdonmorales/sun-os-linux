## Sun/OS v2021.02

Sun/OS هو نظام تشغيل مجاني يعمل على قاعدة Arch Linux ويحتوي على جنوم كبيئة سطح المكتب الافتراضية

تم نشر كود المصدر هنا ، لقد قمنا بتضمين ArchISO لذلك قمت ببناء Sun/OS نظرًا لأننا لا نملك حاليًا مثبتًا رسوميًا ونثبت .iso. لذلك عليك إنشاء Sun/OS ، لدينا أيضًا مثبت CLI و GUI يأتي بشكل منفصل من ArchISO بحيث يمكنك ** git clone ** ... سنقوم أيضًا بتضمين vhd و ova للتشغيل في Virtualbox

فيما يلي المواصفات:

Kernel: Linux 5.10.1 أو 5.9 للنواة المتداول

الإصدار: v2021.01.01

الإصدار المتداول

مدير الحزم: pacman and pamac

DE (بيئة سطح المكتب): جنوم 3.38

البنية: x86_64 (AMD و Intel)

## Sun/OS Installer v2021.02
للحصول على مثبت Sun/OS لتثبيت Sun/OS ، انتقل إلى هذا الرابط لمزيد من المعلومات 
https://github.com/SunOS-Linux/SunOS-Installer

(C) 2021 Morales Research Corp
